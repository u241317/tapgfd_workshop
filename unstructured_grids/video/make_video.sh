#!/bin/bash

base='reg__ke'
fname="./${base}.mp4"

if [ -f ${fname} ]; then
  rm ${fname}
fi

conda activate tapgfd

ffmpeg -start_number 0 -r 7 -f image2 -i ${base}_%04d.jpg -c:v mpeg4 -b:v 12000k ${fname}

du -sh ${fname}
