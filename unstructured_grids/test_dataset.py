# %%
# import intake
import xarray as xr
import zarr
import intake
import dask
# %%
cat=intake.open_catalog(
    "https://swift.dkrz.de/v1/dkrz_7fa6baba-db43-4d12-a295-8e3ebb1a01ed/visualization/intake_dkrz-cloud_eerie_icon_eerie-control-1950.yaml"
)
dsdict={}
for ds in list(cat):
    print(ds)
    dsdict[ds]=cat[ds].to_dask()

print(list(dsdict))

# %% Test reg grid data
ds_sel_reg = dsdict['atmos_gr025_plev19_monthly_mean']
ds_sel_reg
# %%
ds_sel_reg.isel(time=0).isel(plev_2=10).ta.plot()

# %% Test native grid data
ds_grid = dsdict['ocean_native_2d_grid']
ds_grid

# %%
path_2_data = '/Users/mepke/python/tapgfd/data/swift.dkrz.de/tapgfd_workshop/icon-esm-er.eerie-control-1950.ocean.native.2d_grid_minimal.nc'
ds_grid     = xr.open_dataset(path_2_data)
ds_grid
# %%
ds_sel = dsdict['ocean_native_2d_daily_mean']
ds_sel

# %%
#icon cells
import numpy as np
model_lon_icon   = ds_grid.clon.values*180./np.pi
model_lat_icon   = ds_grid.clat.values*180./np.pi
# data_sample_icon = dataICON.to[0,0,:].values # to (time, depth, ncells) float32 ...
data_sample_icon = ds_sel.to.isel(time=0).isel(depth=0).squeeze().values # to (time, depth, ncells) float32 ...

# %%
import matplotlib.pylab as plt
import cmocean
sstep = 20
plt.figure(figsize=(20,10))
plt.scatter(model_lon_icon[::sstep], model_lat_icon[::sstep], s=1 , c=data_sample_icon[::sstep], cmap=cmocean.cm.thermal)
plt.colorbar(orientation='horizontal', pad=0.04)

# %%
